import * as types from '../constants/ActionTypes';
import { Phone } from '../State/Phone';

import axios from 'axios';

export const fetchPhoneList = () => {
    return (
        dispatch: (arg0: { type: string; result: Phone[] }) => void,
    ) => {
        axios.get('http://localhost:3004/phones')
            .then((result) => {
                dispatch({
                    type: types.FETCH_DATA,
                    result: result.data,
                });
            })
            .catch(error => {
                dispatch({
                    type: types.FETCH_DATA,
                    result: [],
                });
            });
    };
};

export const selectPhone = (phone: Phone) => ({
    type: types.SELECT_PHONE,
    phone,
});