import { Phone } from './Phone';

export interface PhoneState {
    phoneList: Phone[];
    selected: Phone | null;
    loading: boolean;
}
