import React from 'react';
import './css/App.css';
import Phone from './components/Phone';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Header from './components/Common/Header';
import Typography from '@material-ui/core/Typography';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        text: {
            color: 'darkslateblue !important',
            textDecoration: 'none !important',
            textAlign: 'center',
        },
    }),
);

function App() {
    const classes = useStyles();

    return (
        <Router>
            <Header />
            <Switch>
                <Route exact path="/">
                    <Typography variant="h3" className={classes.text}>
                        Welcome to Riina Korpela's test
					</Typography>
                    <p className={classes.text}>
                        This is my home page. If you want to see the phone list,{' '}
                        <Link to="/PhoneList">click here!</Link>
                    </p>
                </Route>
                <Route path="/PhoneList">
                    <Phone />
                </Route>
            </Switch>
        </Router>
    );
}

export default App;
