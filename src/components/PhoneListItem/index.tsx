import React from 'react';

import './PhoneListItem.css';
import { useDispatch, useSelector } from 'react-redux';
import { selectPhone } from '../../actions/PhoneAction';
import { Phone } from '../../State/Phone';

export default function PhoneListItem(props: {
    phone: Phone;
}) {
    const selected = useSelector((state: any) => state.phone.selected);
    const { id, name, imageFileName } = props.phone;
    const dispatch = useDispatch();
    return (
        <div
            onClick={() => dispatch(selectPhone(props.phone))}
            className={
                (selected && id === selected.id ? 'selected ' : '') + 'phone-list-item'
            }
        >
            <img
                className="phone-photo"
                src={'../../images/' + imageFileName}
                alt={name}
            />
            <div className="phone-info">
                <h1 className="phone-title">
                    {name}
                </h1>
            </div>
        </div>
    );
}
