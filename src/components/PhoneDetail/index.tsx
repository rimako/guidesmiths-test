import React from 'react';

import './PhoneDetail.css';
import { useSelector } from 'react-redux';
import { Phone } from '../../State/Phone';
import PhoneDetailHeader from './PhoneDetailHeader';
import PhoneDetailTable from './PhoneDetailTable';

export default function PhoneDetail() {
    const selected: Phone = useSelector((state: any) => state.phone.selected);
    if (!selected) {
        return null;
    }
    const { name, imageFileName, description, color, manufacturer, price, processor, ram, screen } = selected;
    return (
        <div
        >
            <PhoneDetailHeader
                name={name}
                price={price}
                manufacturer={manufacturer}
            />
            <div className="phone-info-detail-left">
                <img
                    className="phone-photo-detail"
                    src={'../../images/' + imageFileName}
                    alt={name}
                />
            </div>
            <div className="phone-info-detail-right">
                <p className="phone-description-detail">
                    {description}
                </p>

                <PhoneDetailTable
                    color={color}
                    ram={ram}
                    processor={processor}
                    screen={screen}
                />
            </div>
        </div>
    );
}
