import React from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {
    createStyles,
    Theme,
    makeStyles,
} from '@material-ui/core/styles';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        title: {
            flexGrow: 1,
            display: 'block',
        },
        titleRight: {
            float: 'right'
        },
        header: {
            color: 'white',
            backgroundColor: 'cadetblue'
        }
    }),
);

export default function PhoneDetailHeader(props: {
    name: string;
    manufacturer: string;
    price: number;
}) {
    const name = props.name;
    const price = props.price;
    const manufacturer = props.manufacturer;
    const classes = useStyles();


    return (
        <div className={classes.root}>
            <header className={classes.header}>
                <Toolbar>
                    <Typography className={classes.title} variant="h6" noWrap>
                        {name}
                        <Typography className={classes.title} variant="caption" noWrap>
                            {'by ' + manufacturer}
                        </Typography>
                    </Typography>
                    <Typography className={classes.titleRight} variant="h6" noWrap>
                        {(price).toLocaleString(navigator.language, { style: 'currency', currency: 'EUR' })}
                    </Typography>
                </Toolbar>
            </header>
        </div>
    );
}
