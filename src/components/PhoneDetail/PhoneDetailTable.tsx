import React from 'react';
import { withStyles, Theme, createStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const StyledTableCell = withStyles((theme: Theme) =>
    createStyles({
        head: {
            backgroundColor: 'blue',
            color: theme.palette.common.white,
        },
        body: {
            fontSize: 14,
        },
    }),
)(TableCell);



const useStyles = makeStyles({
    table: {
        maxWidth: '100%'
    },
});

export default function PhoneDetailTable(props: { color: string, ram: number, processor: string, screen: string }) {
    const classes = useStyles();

    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="customized table">
                <TableHead>
                    <TableRow>
                        <StyledTableCell>RAM</StyledTableCell>
                        <StyledTableCell align="right">Processor</StyledTableCell>
                        <StyledTableCell align="right">Screen</StyledTableCell>
                        <StyledTableCell align="right">Color</StyledTableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow key={props.ram}>
                        <StyledTableCell component="th" scope="row">
                            {props.ram}
                        </StyledTableCell>
                        <StyledTableCell align="right">{props.processor}</StyledTableCell>
                        <StyledTableCell align="right">{props.screen}</StyledTableCell>
                        <StyledTableCell align="right">{props.color}</StyledTableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    );
}