import React from 'react';
import PhoneList from '../PhoneList';
import './Phone.css';
import PhoneDetail from '../PhoneDetail';

export default function Phone() {

    return (
        <div className="messenger scrollable">
            <div className="scrollable sidebar">
                <PhoneList />
            </div>

            <div className="scrollable content">
                <PhoneDetail />
            </div>
        </div>
    );
}