import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import { NavLink } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        title: {
            flexGrow: 1,
            color: 'cadetblue !important',
            textDecoration: 'none !important',
        },
        appBar: {
            marginBottom: '10px',
        },
    }),
);

export default function MenuAppBar() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar className={classes.appBar} position="static" color="transparent">
                <Toolbar>
                    <Typography variant="h6" className={classes.title}>
                        <NavLink
                            className={classes.title}
                            isActive={() => {
                                return false;
                            }}
                            to="/PhoneList"
                        >
                            Phone List
						</NavLink>
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}
