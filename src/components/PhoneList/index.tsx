import React, { useEffect } from 'react';
import PhoneListItem from '../PhoneListItem';
import { useDispatch, useSelector } from 'react-redux';

import './PhoneList.css';
import { Phone } from '../../State/Phone';
import { fetchPhoneList } from '../../actions/PhoneAction';
import Loading from '../Loading';

export default function PhoneList() {
    const loading: boolean = useSelector((state: any) => state.phone.loading);

    const phonelist = useSelector(
        (state: any) => state.phone.phoneList,
    );
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchPhoneList());
        // eslint-disable-next-line
    }, []);

    if (loading) {
        return (<Loading />);
    }

    return (
        <div className="phone-list">
            {phonelist &&
                phonelist.map((phone: Phone) => (
                    <PhoneListItem
                        key={phone.id}
                        phone={phone}
                    />
                ))}
        </div>
    );
}
