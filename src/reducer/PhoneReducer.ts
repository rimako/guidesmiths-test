import {
    FETCH_DATA,
    SELECT_PHONE,
} from '../constants/ActionTypes';
import { PhoneState } from '../State/State';
import { Phone } from '../State/Phone';

const initialState: PhoneState = {
    phoneList: [],
    selected: null,
    loading: true
};

export default function phone(
    state = initialState,
    action: { type: any; result: Phone[]; phone: Phone; },
) {
    switch (action.type) {
        case FETCH_DATA: {
            return {
                ...state,
                phoneList: action.result,
                loading: false,
            };
        }
        case SELECT_PHONE:
            return {
                ...state,
                selected: action.phone,
            };
        default:
            return state;
    }
}
