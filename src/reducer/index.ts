import { combineReducers } from 'redux';
import phone from './PhoneReducer';

const rootReducer = combineReducers({
    phone,
});

export default rootReducer;
