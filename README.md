# README #

Made by Riina Maria Korpela.

### What is this repository for? ###

* Test listing phones and visualize their information.

### GENERAL - How do I get set up? ###

* There are two proyects relationed.
* API: https://bitbucket.org/rimako/guidesmiths-api/src/master/
* Front-end: https://bitbucket.org/rimako/guidesmiths-test/src/master/

### How do I get set up API? ###
* Must have installed: node and express.
* To run the app you must run "node index.js" in the root path of the backend proyect.
* The api will run in localhost:3004.

### How do I get set up Frontend? ###
* Must have installed: node to run and yarn to install.
* To run the app you must run "yarn install" in the root path of the frontend proyect.
* After installing the dependencies you can run the app with "yarn start".
* The app will run in localhost:3000.

